# dmenu-scripts-willy

Forked from DT's Gitlab.

# Dmenu Scripts (dmscripts)

The scripts included in this repo are:

+ dm-bookman - Search your qutebrowser bookmarks, quickmarks and history urls.
+ dm-colpick - Copy a color's hex value to your clipboard
+ dm-confedit - Choose from a list of configuration files to edit.
+ dm-currencies - Convert prices between currencies.
+ dm-dictionary - Simple dictionary script
+ dm-documents - Searches for pdf files and opens them with the pdf viewer
+ dmhub - A hub from where you can run all the scripts from.
+ dm-ip - Get IP of interface or external IP
+ dm-kill - Search for a process to kill.
+ dm-logout - Logout, shutdown, reboot or lock screen.
+ dm-maim - A GUI to maim using dmenu.
+ dm-man - Search for a manpage or get a random one.
+ dm-music - Dmenu as your music player
+ dm-note - Store multiple one-line texts or codes and copy one of them when needed.
+ dm-pipewire-out-switcher - Switch default output for pipewire
+ dm-radio - Choose between online radio stations with dmenu.
+ dm-record - Records audio, video and webcam.
+ dm-reddit - Dmenu as a reddit viewer using reddio. *STILL A WORK IN PROGRESS*
+ dm-setbg - A wallpaper setting utility using dmenu, xwallpaper and sxiv
+ dm-sounds - Choose an ambient background to play.
+ dm-spellcheck - Script to check spellings
+ dm-translate - Translate using Google Translate (through Lingva Translate)
+ dm-usbmount - mount/unmount usb drives using dmenu. No fancy daemon required
+ dm-weather - Simple graphical weather app
+ dm-websearch - Search various search engines (inspired by surfraw).
+ dm-wifi - Connect to wifi using dmenu.
+ dm-wiki - Search an offline copy of the Arch Wiki.
+ dm-youtube - Youtube subscriptions without an account or the API tying you down.
+ \_dm-helper.sh Helper scripts adding functionality to other scripts

![Screenshot of dmenu](https://gitlab.com/dwt1/dotfiles/raw/master/.screenshots/dmenu-distrotube01.png)

# IMPORTANT!

When we write scripts, we test with a default dmenu configuration. This means potential issues with other patches or dmenu alternatives may be missed by us. Please feel free to patch the program yourself or see if you can figure out why the patch/program is incompatible and either take it upstream or fix your version.

## Patches to avoid:

+ Case insensitive 

Case insensitive is a patch that removes the -i flag and makes insensitive casing the default
behaviour, this needs to be reverted if you wish to use dmscripts in the default configuration.

## Programs to avoid:

Currently, none.

# Dependencies

Of course, dmenu is a dependency for all of these scripts.  To see the dependencies of each individual script, check the top commented block of text in each script. For installing you will need pandoc and, of course, git.

# Installation

## Installation on Arch

If you are using Arch, clone the repository then go through the manual build process to install the scripts. Run the following commands:

```bash
$ git clone https://gitlab.com/willy45/dmscripts.git
$ cd dmenu_scripts
$ makepkg -cf -si
```

## Installation on Other Linux Distributions

All you need to do is clone this repository and run setup. Run the following commands:

```bash
$ git clone https://gitlab.com/dwt1/dmenu-scripts-willy.git
$ cd dmenu-scripts-willy
$ sudo make install
```

Once installed, the scripts should behave like any other command and can be run by typing the script's name. It is important to note however that the dependencies are not installed by default, that is up to YOU to do before installing.

NOTE: Some distributions require the Haskell programming language to be installed as pandoc is a Haskell program. If you wish to use the software without installing Haskell, we recommend [downloading a static build](https://github.com/jgm/pandoc/releases). A guide for installation can be found [on the pandoc github](https://github.com/jgm/pandoc/blob/2.14.0.3/INSTALL.md).

## Non-installation

If you wish to try the scripts without installing, you can use dm-hub:
for the scripts to work you need to have the config-file in one of three locations:
+ /etc/dmscripts/config
+ ../config/config (path relative to scripts in git-repo)
+ ~/.config/dmscripts/config

```bash
$ bash /path/to/dm-hub
```

To run a script without using the hub:

```bash
$ bash /path/to/script
```

Or:

```bash
$ ./path/to/script
```

# Configuration

Currently, configuration can be done in a few ways:
+ copying config (from repo ./config/config or /etc/dmscripts/config if installed) to ~/.config/dmscripts/config (Recommended)
+ Via the global config file `/etc/dmscripts/config` (will cause diff when updating)
	+ Maintenance
+ Via editing the source code (not recommended)
	+ Changing the Config Location

## The Global Config

Currently only a "global" config is installed to `/etc/dmscripts/config`.
To install a user-specific version of the config run the following command:

```bash
$ cp -riv config/ "$HOME"/.config/dmscripts
```

The config file is a bash script however it is very simple to understand and several comments are left which explains what everything in the config file does. If you are still confused, a general word of advice that you should just copy one of the lines in the config and modify it to see what it does.

### Changing the Config Location

If you could like to change config location you can add custom search path into the array in function `get_config` in `_dm-helper.sh`

```bash
config_dirs+=(
"/path/to/custom/config-file"
"${HOME}/.config/dmscripts/config"
"/etc/dmscripts/config"
)
```
